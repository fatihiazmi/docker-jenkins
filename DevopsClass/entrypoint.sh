#!/bin/sh

# wait for the MySQL server to be available
while ! nc -z db 3306; do
  echo "Waiting for the MySQL server..."
  sleep 1
done

# run the Django server
exec "$@"